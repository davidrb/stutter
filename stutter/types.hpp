#ifndef STUTTER_TYPES_HPP
#define STUTTER_TYPES_HPP

#include <string>

#define STUTTER_Q(x) #x
#define STUTTER_QUOTE(x) STUTTER_Q(x)

#define lisp_define_symbol(n) \
  namespace lisp_symbols { struct n { static constexpr auto name = std::string_view{STUTTER_QUOTE(n)}; }; }; \
  using n = sym<::lisp_symbols::n>;

struct nil { static auto to_s() { return std::string{"nil"}; } };
struct ifte { static auto to_s() { return std::string{"ifte"}; } };
struct plus { static auto to_s() { return std::string{"plus"}; } };
struct mul { static auto to_s() { return std::string{"mul"}; } };
struct less { static auto to_s() { return std::string{"less"}; } };
struct eq { static auto to_s() { return std::string{"eq"}; } };
struct car { static auto to_s() { return std::string{"car"}; } };
struct cdr { static auto to_s() { return std::string{"cdr"}; } };
struct cons { static auto to_s() { return std::string{"fals"}; } };
struct tru { static auto to_s() { return std::string{"tru"}; } };
struct fals { static auto to_s() { return std::string{"fals"}; } };

template<typename... syms>
struct lambda { static auto to_s() { return "lambda"; } };

template<typename x> struct sym {
  static auto to_s() -> std::string {
    return std::string{"sym<"} + std::string{x::name} + ">";
  }
};

template<long n> struct num {
  static auto to_s() {
    return std::string{"num<"} + std::to_string(n) + ">";
  }
};

template<typename... es> struct list {
  static auto to_s() { return std::string{"list<>"}; }
};

template<typename x> struct list<x> {
  static auto to_s() -> std::string { return std::string{"list<"} + x::to_s() + ">"; }
  static auto to_s__() -> std::string { return std::string{", "} + x::to_s() + ">"; }
};

template<typename x, typename... xs> struct list<x, xs...> {
  static auto to_s() -> std::string { return std::string{"list<"} + x::to_s() + list<xs...>::to_s__(); }
  static auto to_s__() -> std::string { return std::string{", "} + x::to_s() + list<xs...>::to_s__(); }
};

template<typename T> struct quote {
  static auto to_s() -> std::string { return std::string{"quote<"} + T::to_s() + ">"; }
};

template<typename syms, typename b>
struct closure {
  using sym_list = syms;
  using body = b;

  static auto to_s() -> std::string {
    return std::string{"closure<"} + syms::to_s() + std::string{", "} + body::to_s() + std::string{">"};
  }
};

#include <type_traits>
#define lisp_assert_eq(...) static_assert(std::is_same<__VA_ARGS__>::value);

#endif
