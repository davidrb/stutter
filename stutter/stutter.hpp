#ifndef STUTTER_HPP
#define STUTTER_HPP

#include "types.hpp"

template<typename x, typename l> struct append__;
template<typename x, typename... elems>
struct append__<x, list<elems...>> {
  using value = list<x, elems...>;
};

template<typename v>
struct eval { using value = v; };

template<typename v>
struct eval<quote<v>> { using value = v; };

template<typename b, typename t, typename e>
struct eval<list<ifte, b, t, e>> {
  using value = typename eval<list< ifte, typename eval<b>::value, t, e >>::value;
};

template<typename t, typename e>
struct eval<list<ifte, tru, t, e>> {
  using value = typename eval<t>::value;
};

template<typename t, typename e>
struct eval<list<ifte, fals, t, e>> {
  using value = typename eval<e>::value;
};

template<typename b, typename t, typename e>
struct eval<list<ifte, b, t, e>>;

template<typename n1, typename n2>
struct eval<list<plus, n1, n2>> {
  using value = typename eval<
    list<plus, typename eval<n1>::value,
               typename eval<n2>::value>>::value;
};

template<long n1, long n2>
struct eval<list<plus, num<n1>, num<n2>>> {
  using value = num<n1 + n2>;
};

template<typename n1, typename n2>
struct eval<list<mul, n1, n2>> {
  using value = typename eval<
    list<mul, typename eval<n1>::value,
               typename eval<n2>::value>>::value;
};

template<long n1, long n2>
struct eval<list<mul, num<n1>, num<n2>>> {
  using value = num<n1 * n2>;
};

template<typename op, typename... args>
struct eval<list<op, args...>> {
  using value = typename eval<list< typename eval<op>::value, args... >>::value;
};

template<typename arg>
struct quote_if_list {
  using value = arg;
};

template<typename... es>
struct quote_if_list<list<es...>> {
  using value = quote<list<es...>>;
};

template<typename sym, typename arg, typename body>
struct bind_arg { using value = body; };

template<typename sym, typename arg>
struct bind_arg<sym, arg, sym> { using value = arg; };

template<typename sym, typename arg>
struct bind_arg<sym, arg, list<>> {
  using value = list<>;
};

template<typename sym, typename arg, typename first_e, typename... elems>
struct bind_arg<sym, arg, list<first_e, elems...>> {
  using value = typename append__<
        typename bind_arg<sym, arg, first_e>::value,
        typename bind_arg<sym, arg, list<elems...>>::value >::value;
};

template<typename sym_list, typename args, typename body>
struct bind_args;

template<typename body>
struct bind_args< list<>, list<>, body> {
  using value = body;
};

template<typename body, typename first_sym, typename first_arg, typename... rest_syms, typename... rest_args>
struct bind_args< list<first_sym, rest_syms...>, list<first_arg, rest_args...>, body> {
  using value = typename bind_args<
    list<rest_syms...>,
    list<rest_args...>,
    typename bind_arg<first_sym, typename quote_if_list<first_arg>::value, body>::value>::value;
};

template<typename sym_list, typename body, typename... args>
struct eval<list<closure<sym_list, body>, args...>> {
  using value = typename eval<
    typename bind_args< sym_list, list<typename eval<args>::value...>, body >::value
  >::value;
};

template<typename... syms, typename body>
struct eval<list<lambda<syms...>, body>> {
  using value = closure<list<syms...>, body>;
};

template<typename... syms, typename b1, typename b2, typename... body>
struct eval<list<lambda<syms...>, b1, b2, body...>> {
  using value = closure<list<syms...>, list<b1, b2, body...>>;
};

template<typename v1, typename v2>
struct equal__ { using value = fals; };

template<typename v>
struct equal__<v, v> { using value = tru; };

template<typename v1, typename v2>
struct eval<list<eq, v1, v2>> {
  using value = typename equal__<
    typename eval<v1>::value,
    typename eval<v2>::value>::value;
};

template<typename l> struct car__;
template<typename e, typename... es>
struct car__<list<e, es...>> { using value = e; };
template<> struct car__<list<>> { using value = nil; };

template<typename l>
struct eval<list<car, l>> {
  using value = typename car__< typename eval<l>::value >::value;
};

template<typename l> struct cdr__;
template<typename e, typename... es>
struct cdr__<list<e, es...>> { using value = list<es...>; };
template<> struct cdr__<list<>> { using value = nil; };

template<typename l>
struct eval<list<cdr, l>> {
  using value = typename cdr__<typename eval<l>::value>::value;
};

template<typename e, typename l> struct cons__;
template<typename e, typename... es>
struct cons__<e, list<es...>> { using value = list<e, es...>; };
template<typename e> struct cons__<e, nil> { using value = list<e>; };

template<typename e, typename l>
struct eval<list<cons, e, l>> {
  using value = typename cons__< typename eval<e>::value, typename eval<l>::value >::value;
};

template<typename n1, typename n2>
struct eval<list<less, n1, n2>> {
  using value = typename eval< list<less, typename eval<n1>::value, typename eval<n2>::value> >::value;
};

template<bool> struct to_tru_fals;
template<> struct to_tru_fals<true> { using value = tru; };
template<> struct to_tru_fals<false> { using value = fals; };

template<long n1, long n2>
struct eval<list<less, num<n1>, num<n2>>> {
  using value = typename to_tru_fals<(n2 - n1 > 0)>::value;
};

template<typename... es> struct evaluate__ { using value = typename eval<list<es...>>::value; };
template<typename v> struct evaluate__<v> { using value = typename eval<v>::value; };

template<typename... es>
using evaluate = typename evaluate__<es...>::value;

#endif
