#include "stutter/stutter.hpp"

lisp_define_symbol(x);
lisp_define_symbol(f);
lisp_define_symbol(v);
lisp_define_symbol(r);
lisp_define_symbol(n);
lisp_define_symbol(y);

using Y = evaluate<
  lambda<f>,
    list<lambda<x>, list<f, list<lambda<v>, list<x, x>, v>>>,
    list<lambda<x>, list<f, list<lambda<v>, list<x, x>, v>>>>;

using fibb = evaluate<
  Y, list<lambda<r>,
       list<lambda<n>,
         ifte, list<less, n, num<2>>,
               n,
               list<plus,
                 list<r, list<plus, n, num<-1>>>,
                 list<r, list<plus, n, num<-2>>>>>>>;

lisp_assert_eq( num<0>, evaluate<fibb, num<0>> );
lisp_assert_eq( num<1>, evaluate<fibb, num<1>> );
lisp_assert_eq( num<6765>, evaluate<fibb, num<20>> );

#include <iostream>

int main() {
  std::cout << evaluate<fibb, num<80>>::to_s() << '\n';

  using size = evaluate<
    Y, list<lambda<r>,
         list<lambda<n>,
           ifte, list<eq, n, quote<list<>>>,
                 num<0>,
                 list<plus, num<1>, list<r, list<cdr, n>>>>>>;

  lisp_assert_eq(num<5>, evaluate<size, quote<list<x, y, x, y, x>>>);
  std::cout << evaluate<size, quote<list<y, x, y, y, x>>>::to_s() << "\n";
}
